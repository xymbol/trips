import Ember from 'ember';

export default Ember.ObjectController.extend({
  isValid: Ember.computed('name', function() {
    return !Ember.isEmpty(this.get('name'));
  }),
  actions: {
    save: function() {
      if (this.get('isValid')) {
        var self = this;
        this.get('model').save().then(function(trip) {
          console.log(trip);
          self.transitionToRoute('trips.index');
        });
      } else {
        // ...
      }
    }
  }
});
