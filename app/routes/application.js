import Ember from 'ember';

export default Ember.Route.extend({
  setupController: function() {
    this.controllerFor('trips/new').set('model', this.store.createRecord('trip'));
  }
});
